#region

using RPGCore.Stats.Events;
using UnityEngine;

#endregion

namespace Sample.Scripts
{
    public class SampleStatEventHandler : StatUpdateObserver
    {
    #region Public Methods

        public void OnStatValueUpdated(string statName , float finalValue)
        {
            Debug.Log($"StatMediator: {statName} , {finalValue}");
        }

    #endregion
    }
}