#region

using Zenject;

#endregion

namespace Sample.Scripts
{
    public class SampleInstaller : MonoInstaller
    {
    #region Public Methods

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<SampleStatEventHandler>().AsSingle();
            Container.BindInterfacesTo<SampleInitialization>().AsSingle();
            Container.BindFactory<PlayerCharacter , PlayerCharacter.Factory>()
                     .FromSubContainerResolve()
                     .ByNewPrefabResourceInstaller<PlayerCharacterInstaller>("PlayerCharacter");
        }

    #endregion
    }
}