#region

using Zenject;

#endregion

namespace Sample.Scripts
{
    public class PlayerCharacterInstaller : Installer<PlayerCharacterInstaller>
    {
    #region Public Methods

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<PlayerStatEventHandler>().AsSingle();
            Container.BindInterfacesTo<PlayerCharacterInitialization>().AsSingle();
            Container.Bind<PlayerCharacter>().FromNewComponentOnRoot().AsSingle().NonLazy();
        }

    #endregion
    }
}