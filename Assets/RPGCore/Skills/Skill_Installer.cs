#region

using rStarUtility.Util.Extensions.CSharp;
using Zenject;

#endregion

namespace RPGCore.Skills
{
    public class Skill_Installer : Installer<Skill_Installer>
    {
    #region Public Methods

        public override void InstallBindings()
        {
            Container.BindFactory<Skill.SpawnData , Skill , Skill.Factory>();
            Container.Bind<SkillRepository>().AsSingle().IfNotBound();
            Container.Bind<SkillController>().AsSingle().IfNotBound();

            if (Container.HasBinding<SkillTicker>().IsFalse()) Container.BindInterfacesAndSelfTo<SkillTicker>().AsSingle();
            Container.BindInterfacesAndSelfTo<TimeProvider>().AsSingle().IfNotBound();
        }

    #endregion
    }
}