#region

using System.Collections.Generic;
using Sample.Scripts.DataStructure;
using UnityEngine;

#endregion

namespace Sample.Scripts
{
    [CreateAssetMenu(fileName = "SampleDataSO" , menuName = "SampleDataSO" , order = 0)]
    public class SampleDataSO : ScriptableObject
    {
    #region Public Variables

        public List<StatData> statDatas = new List<StatData>();

    #endregion
    }
}