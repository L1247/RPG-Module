namespace RPGCore.Skills.Events
{
    public interface ExecuteObserver
    {
    #region Public Methods

        void OnSkillExecuted(string skillId);

    #endregion
    }
}