#region

using RPGCore.Items.Events;
using RPGCore.Items.Main;
using Zenject;

#endregion

namespace RPGCore.Items.Handlers
{
    public class UnRegisterHandler : DeSpawnObserver
    {
    #region Private Variables

        [Inject]
        private ItemRepository repository;

    #endregion

    #region Public Methods

        public void OnDeSpawn(string id)
        {
            var remove = repository.Remove(id);
        }

    #endregion
    }
}