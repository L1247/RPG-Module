namespace RPGCore.Skills.Interfaces
{
    public interface Damageable
    {
    #region Public Methods

        void TakeDamage(int damage);

    #endregion
    }
}