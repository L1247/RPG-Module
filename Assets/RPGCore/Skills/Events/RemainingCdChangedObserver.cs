namespace RPGCore.Skills.Events
{
    public interface RemainingCdChangedObserver
    {
    #region Public Methods

        void OnRemainingCdChanged(string skillId , float remainingCd);

    #endregion
    }
}