#region

using System;
using System.Collections.Generic;
using RPGCore.Modifiers;
using Sirenix.OdinInspector;

#endregion

namespace Sample.Scripts.DataStructure
{
    [Serializable]
    [HideLabel]
    public class ModifierGroup
    {
    #region Public Variables

        [LabelText("素質加成")]
        public List<ModifierData> modifierDatas = new List<ModifierData>();

    #endregion

    #region Public Methods

        public void AddModifierData(string statName , ModifierType modifierType = ModifierType.Flat , float value = 0)
        {
            modifierDatas.Add(new ModifierData(statName , modifierType , value));
        }

    #endregion
    }
}