#region

using NSubstitute;
using NUnit.Framework;
using RPGCore.Items.Events;
using RPGCore.Items.Handlers;
using RPGCore.Items.Main;
using rStarUtility.Generic.TestExtensions;
using rStarUtility.Generic.TestFrameWork;
using UnityEngine;
using Zenject;

#endregion

public class ItemTests : DIUnitTestFixture
{
#region Private Variables

    private readonly string itemDataId = "Item.DataId";

#endregion

#region Test Methods

    [Test(Description = "建立道具")]
    [Category("Event")]
    public void _01_Create_Item()
    {
        var spawnObserver = Bind_Mock_And_Resolve<SpawnObserver>();
        var dataId        = "DataId";
        var spawnInfo     = Item.SpawnInfo.Create(dataId , Vector2.one);
        Bind<Item>();
        var item = Resolve<Item>();

        item.OnSpawned(spawnInfo);

        item.Position.ShouldBe(Vector2.one);
        item.Id.ShouldNotBeNull();
        item.DataId.ShouldBe(dataId);
        spawnObserver.Received(1).OnSpawn(Arg.Any<string>() , dataId , Vector2.one);
    }

    [Test(Description = "產生後，註冊到Repository")]
    public void _02_Register_To_Repository_When_Spawn()
    {
        var repository = Bind_And_Resolve<ItemRepository>();
        Bind_InterfacesTo<RegisterHandler>();

        Bind_And_Resolve<Item>().OnSpawned(Item.SpawnInfo.CreateEmpty());

        repository.Count.ShouldBe(1);
    }

    [Test(Description = "銷毀後，從Repository移除")]
    public void _03_UnRegister_To_Repository_When_DeSpawn()
    {
        Bind_InterfacesTo<UnRegisterHandler>();
        Bind<Item>();
        Bind<ItemRepository>();

        Resolve<Item>().OnDespawned();

        var itemRepository = Resolve<ItemRepository>();
        itemRepository.Count.ShouldBe(0);
    }

    [Test(Description = "只能Dispose一次有效")]
    [Category("Zenject")]
    public void _04_Dispose_Only_Once()
    {
        var pool = Bind_Mock_And_Resolve<IMemoryPool>();
        var item = Bind_And_Resolve<Item>();

        item.Dispose();
        pool.Received(1).Despawn(item);
        item.Disposed.ShouldBe(true);

        pool.ClearReceivedCalls();
        item.Dispose();
        pool.Received(0).Despawn(item);
    }

    [Test(Description = "道具被撿起")]
    [Category("Event")]
    public void _05_PickUp()
    {
        Bind_Instance(itemDataId , itemDataId);
        var observer = Bind_Mock_And_Resolve<PickUpObserver>();
        var item     = Bind_And_Resolve<Item>();

        item.PickUp();

        item.Disposed.ShouldBe(true);
        observer.Received(1).OnItemPickUp(item.Id , itemDataId);
    }

#endregion
}