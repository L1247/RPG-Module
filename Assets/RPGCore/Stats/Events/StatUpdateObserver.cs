namespace RPGCore.Stats.Events
{
    public interface StatUpdateObserver
    {
    #region Public Methods

        void OnStatValueUpdated(string statName , float finalValue);

    #endregion
    }
}