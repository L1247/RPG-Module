namespace RPGCore.Stats.Events
{
    public interface StatCreateObserver
    {
    #region Public Methods

        void OnStatCreated(string statName , float baseValue);

    #endregion
    }
}