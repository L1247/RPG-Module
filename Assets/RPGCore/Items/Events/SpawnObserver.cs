#region

using UnityEngine;

#endregion

namespace RPGCore.Items.Events
{
    public interface SpawnObserver
    {
    #region Public Methods

        void OnSpawn(string id , string dataId , Vector2 spawnPos);

    #endregion
    }
}