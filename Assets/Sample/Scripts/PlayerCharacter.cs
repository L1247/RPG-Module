#region

using UnityEngine;
using Zenject;

#endregion

namespace Sample.Scripts
{
    public class PlayerCharacter : MonoBehaviour
    {
    #region Public Methods

        public void LogStatValue()
        {
            // var statValue = statController.GetValue("Atk");
            // Debug.Log($"{statValue.statTotalValue}");
            // statValue = statController.GetValue("NonStat");
            // Debug.Log($"{statValue.statTotalValue}");
        }

    #endregion

    #region Nested Types

        public class Factory : PlaceholderFactory<PlayerCharacter> { }

    #endregion
    }
}