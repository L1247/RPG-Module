namespace RPGCore.Items.Events
{
    public interface PickUpObserver
    {
    #region Public Methods

        void OnItemPickUp(string itemId , string itemDataId);

    #endregion
    }
}