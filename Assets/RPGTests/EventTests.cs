#region

using NSubstitute;
using NUnit.Framework;
using RPGCore.Skills.Events;

#endregion

[TestFixture]
public class EventTests : SkillTests
{
#region Test Methods

    [Test(Description = "發送事件，數值相同沒改變不發事件")]
    [Category("Event")]
    public void _01_Tick_Skill_Time_Notify_Observer()
    {
        var observer = Bind_Mock_And_Resolve<RemainingCdChangedObserver>();
        var skill    = Create_Skill(1);
        skill.TickTime(0.5f);
        observer.Received(1).OnRemainingCdChanged(skillId , 0.5f);
        observer.ClearReceivedCalls();

        skill.TickTime(0);
        observer.DidNotReceive().OnRemainingCdChanged(Arg.Any<string>() , Arg.Any<float>());
    }

    [Test(Description = "由Tick觸發，執行觸發執行技能事件")]
    [Category("Event")]
    public void _02_Tick_Skill_Time_Notify_Execute_Observer()
    {
        var observer = Bind_Mock_And_Resolve<ExecuteObserver>();
        var skill    = Create_Skill(1);

        skill.TickTime(1f);
        observer.Received(1).OnSkillExecuted(skillId);
        observer.ClearReceivedCalls();

        skill.TickTime(1);
        // case skill reset.
        observer.Received(1).OnSkillExecuted(skillId);
    }

    [Test(Description = "執行技能，觸發執行技能事件")]
    [Category("Event")]
    public void _03_Execute_Skill_Notify_Execute_Observer()
    {
        var observer = Bind_Mock_And_Resolve<ExecuteObserver>();
        var skill    = Create_Skill(1 , true);

        skill.Execute();

        observer.Received(1).OnSkillExecuted(skillId);
    }

#endregion
}