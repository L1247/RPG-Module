#region

using NSubstitute;
using NUnit.Framework;
using RPGCore.Skills;
using rStarUtility.Generic.TestExtensions;
using rStarUtility.Generic.TestFrameWork;

#endregion

public class SkillControllerTests : DIUnitTestFixture
{
#region Private Variables

    private static string id => "SkillName";

#endregion

#region Test Methods

    [Test(Description = "建立技能")]
    public void _01_1_Create_Skill()
    {
        var controller = Given_Controller();

        controller.Create(id , 1).ShouldSuccess();

        var repository = GetRepository();
        repository.Count.ShouldBe(1);
        repository.Find(id).Present.ShouldBe(true);
        repository.Find(id).Value.Cd.ShouldBe(1);
    }

    [Test]
    public void _01_2_()
    {
        var controller = Given_Controller();

        controller.Create(id , 1).ShouldSuccess();

        var repository = GetRepository();
        repository.Count.ShouldBe(1);
        repository.Find(id).Present.ShouldBe(true);
        repository.Find(id).Value.Cd.ShouldBe(1);
    }

    [Test(Description = "設定CD")]
    public void _02_Set_CD()
    {
        var controller = Given_Controller();
        var skill      = Create_A_Skill(1f);

        controller.SetCD(id , 0.5f).ShouldSuccess();

        skill.Cd.ShouldBe(0.5f);
    }

    [Test(Description = "取得CD")]
    public void _03_Get_CD()
    {
        var controller = Given_Controller();
        Create_A_Skill(999);

        var getCdResult = controller.GetCD(id);

        getCdResult.ShouldSuccess();
        getCdResult.cd.ShouldBe(999);
        getCdResult.baseCd.ShouldBe(999);
    }

    [Test(Description = "暫停技能Tick")]
    public void _04_Pause_Skill_Tick()
    {
        Bind_Skill_Ticker(false);
        Given_Controller().PauseTick();
        var skill = Create_A_Skill(999);

        Resolve<SkillTicker>().Tick();

        skill.RemainingCd.ShouldBe(999);
    }

    [Test(Description = "恢復技能Tick")]
    public void _05_Resume_Skill_Tick()
    {
        Bind_Skill_Ticker(true);
        Given_Controller().ResumeTick();
        var skill = Create_A_Skill(999);

        Resolve<SkillTicker>().Tick();

        skill.RemainingCd.ShouldBe(998);
    }

#endregion

#region Private Methods

    private void Bind_Skill_Ticker(bool pause)
    {
        Bind_Instance(pause , "PauseTick");
        Bind<SkillTicker>();
        Bind_Mock_And_Resolve<ITimeProvider>().GetDeltaTime().Returns(1f);
    }

    private Skill Create_A_Skill(float cd)
    {
        Container.Bind<Skill.SpawnData>().AsSingle().WithArguments(id , cd);
        var skill = Container.Instantiate<Skill>();
        GetRepository().Add(skill);
        return skill;
    }

    private SkillRepository GetRepository()
    {
        return Resolve<SkillRepository>();
    }

    private SkillController Given_Controller()
    {
        InstallSkill();
        return Resolve<SkillController>();
    }

    private void InstallSkill()
    {
        Skill_Installer.Install(Container);
    }

#endregion
}