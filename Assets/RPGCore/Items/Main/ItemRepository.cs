#region

using System.Collections.Generic;
using rStarUtility.Generic.Infrastructure;

#endregion

namespace RPGCore.Items.Main
{
    public class ItemRepository : Repository<Item>
    {
    #region Constructor

        public ItemRepository(List<Item> entities) : base(entities) { }

    #endregion
    }
}