#region

using System;
using System.Collections.Generic;
using RPGCore.Skills.Events;
using rStarUtility.Generic.Infrastructure;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;
using Math = rStarUtility.Util.Helper.Math;

#endregion

namespace RPGCore.Skills
{
    public class Skill : IEntity<string>
    {
    #region Public Variables

        /// <summary>
        ///     原本產生時的CD
        /// </summary>
        public float BaseCD { get; }

        public float Cd { get; private set; }

        /// <summary>
        ///     經過CD時間
        /// </summary>
        public float ElapsedTime => Cd - RemainingCd;

        /// <summary>
        ///     剩餘CD時間
        /// </summary>
        [Inject(Optional = true , Id = "RemainingCd")]
        public float RemainingCd { get; private set; }

        public string Id => Name;

        public string Name { get; }

    #endregion

    #region Private Variables

        [Inject]
        private List<RemainingCdChangedObserver> remainingCdChangedObservers;

        [Inject]
        private List<ExecuteObserver> executeObservers;

        private readonly bool noCd;

    #endregion

    #region Constructor

        public Skill(SpawnData spawnData)
        {
            Name = spawnData.Name;
            var cd = spawnData.Cd;
            BaseCD      = cd < 0 ? 0 : cd;
            Cd          = BaseCD;
            RemainingCd = cd;
            noCd        = spawnData.NoCd;
        }

    #endregion

    #region Public Methods

        public void Execute()
        {
            ResetRemainingCD();
            executeObservers.ForEach(_ => _.OnSkillExecuted(Id));
        }

        public void ResetRemainingCD()
        {
            if (noCd) return;
            RemainingCd = Cd;
        }

        public void SetCD(float newCd)
        {
            if (newCd < 0) newCd = 0;

            Cd = newCd;
            if (RemainingCd > Cd) ResetRemainingCD();
        }

        public void TickTime(float seconds)
        {
            var decreaseRemainingCd = RemainingCd - seconds;
            SetRemainingCd(decreaseRemainingCd);
            if (RemainingCd <= 0) Execute();
        }

    #endregion

    #region Private Methods

        private void SetRemainingCd(float remainingCd)
        {
            var lastRemainingCd = RemainingCd;
            var cdNoChange      = Math.Abs(lastRemainingCd - remainingCd) < 0.01f;
            if (cdNoChange) return;
            RemainingCd = Math.Clamp_0_To_Max(remainingCd);
            remainingCdChangedObservers.ForEach(_ => _.OnRemainingCdChanged(Id , RemainingCd));
        }

    #endregion

    #region Nested Types

        public class Factory : PlaceholderFactory<SpawnData , Skill> { }

        public class SpawnData
        {
        #region Public Variables

            public bool   NoCd { get; }
            public float  Cd   { get; }
            public string Name { get; }

        #endregion

        #region Constructor

            public SpawnData(string name , float cd , bool noCd = false)
            {
                Name = name;
                Cd   = cd;
                NoCd = noCd;
            }

        #endregion
        }

    #endregion
    }

    [Serializable]
    public class SkillData
    {
    #region Public Variables

        public float cd = 1;

        [LabelText("基礎攻擊力倍率%")]
        public float power = 100;

        [PropertyOrder(-5)]
        public int lv = 1;

        [PropertyOrder(-90)]
        public Sprite icon;

        [PropertyOrder(-100)]
        [ValueDropdown("@ValueList.GetSkillNames")]
        public string name;

    #endregion
    }
}