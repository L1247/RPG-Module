#region

using rStarUtility.Util.Extensions.CSharp;
using Zenject;

#endregion

namespace RPGCore.Skills
{
    public class SkillTicker : ITickable
    {
    #region Private Variables

        [Inject]
        private SkillRepository skillRepository;

        [Inject]
        private ITimeProvider timeProvider;

        [Inject(Optional = true , Id = "PauseTick")]
        private bool pauseTick;

    #endregion

    #region Public Methods

        public void Pause()
        {
            pauseTick = true;
        }

        public void Resume()
        {
            pauseTick = false;
        }

        public void Tick()
        {
            if (pauseTick) return;
            skillRepository.Entities.ForEach(skill => skill.TickTime(timeProvider.GetDeltaTime()));
        }

    #endregion
    }
}