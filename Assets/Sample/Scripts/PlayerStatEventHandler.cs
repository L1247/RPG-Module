#region

using RPGCore.Stats.Events;
using UnityEngine;

#endregion

namespace Sample.Scripts
{
    public class PlayerStatEventHandler : StatUpdateObserver
    {
    #region Public Methods

        public void OnStatValueUpdated(string statName , float finalValue)
        {
            Debug.Log($"PlayerStatEventHandler: {statName} , {finalValue}");
        }

    #endregion
    }
}