#region

using RPGCore.Items.Events;
using RPGCore.Items.Main;
using UnityEngine;
using Zenject;

#endregion

namespace RPGCore.Items.Handlers
{
    public class RegisterHandler : SpawnObserver
    {
    #region Private Variables

        [Inject]
        private ItemRepository repository;

        [Inject]
        private Item item;

    #endregion

    #region Public Methods

        public void OnSpawn(string id , string dataId , Vector2 spawnPos)
        {
            repository.Add(item);
        }

    #endregion
    }
}