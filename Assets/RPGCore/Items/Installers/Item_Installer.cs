#region

using RPGCore.Items.Handlers;
using RPGCore.Items.Main;
using Zenject;

#endregion

namespace RPGCore.Items.Installers
{
    public class Item_Installer : Installer<Item_Installer>
    {
    #region Public Methods

        public override void InstallBindings()
        {
            Container.BindFactory<Item.SpawnInfo , Item , Item.Factory>()
                     .FromPoolableMemoryPool<Item.SpawnInfo , Item , Item.Pool>(
                              poolBinder =>
                                      poolBinder.WithInitialSize(20)
                                                .FromSubContainerResolve()
                                                .ByMethod(InstallItem));

            Container.Bind<ItemRepository>().AsSingle().IfNotBound();
            Container.Bind<ItemController>().AsSingle().IfNotBound();
        }

    #endregion

    #region Private Methods

        private void InstallItem(DiContainer subContainer)
        {
            subContainer.Bind<Item>().AsSingle();
            subContainer.BindInterfacesTo<RegisterHandler>().AsSingle();
            subContainer.BindInterfacesTo<UnRegisterHandler>().AsSingle();
        }

    #endregion
    }
}