#region

using RPGCore.Skills.Results;
using rStarUtility.Generic.Infrastructure;
using rStarUtility.Util.DDD.UseCase;
using rStarUtility.Util.Extensions.CSharp;
using UnityEngine.Assertions;
using Zenject;

#endregion

namespace RPGCore.Skills
{
    public class SkillController : Controller<Skill , SkillRepository>
    {
    #region Private Variables

        [Inject]
        private Skill.Factory factory;

        [Inject]
        private SkillTicker ticker;

    #endregion

    #region Public Methods

        public CqrsOutput Create(string skillName , float cd)
        {
            RequireId(skillName);
            if (Contains(skillName)) CqrsOutput.Create().Fail().SetMessage($"建立技能失敗，技能[{skillName}]已建立");
            RequiredCD(cd);
            var spawnData  = new Skill.SpawnData(skillName , cd);
            var skill      = factory.Create(spawnData);
            var addSucceed = repository.Add(skill);
            return addSucceed
                           ? CqrsOutput.Create().SetId(skill.Id)
                           : CqrsOutput.Create().Fail().SetMessage($"未知原因，建立技能[{skillName}]失敗");
        }

        public GetCdResult GetCD(string skillName)
        {
            RequireId(skillName);
            var optional = FindEntity(skillName);
            if (optional.Present.IsFalse()) return GetCdResult.Create().Fail().SetMessage($"查詢CD失敗，技能[{skillName}]不存在");

            var skill       = optional.Value;
            var cd          = skill.Cd;
            var baseCd      = skill.BaseCD;
            var getCdResult = GetCdResult.Create().SetId(skill.Id);
            getCdResult.cd     = cd;
            getCdResult.baseCd = baseCd;
            return getCdResult;
        }

        public void PauseTick()
        {
            ticker.Pause();
        }

        public void ResumeTick()
        {
            ticker.Resume();
        }

        public CqrsOutput SetCD(string skillName , float newCD)
        {
            RequireId(skillName);
            RequiredCD(newCD);
            if (Contains(skillName).IsFalse()) return CqrsOutput.Create().Fail().SetMessage($"設定CD失敗，技能{skillName}不存在.");

            var skill = GetEntity(skillName);
            skill.SetCD(newCD);
            var setCdSucceed = skill.Cd == newCD;
            return setCdSucceed
                           ? CqrsOutput.Create().SetId(skill.Id)
                           : CqrsOutput.Create().Fail().SetMessage($"未知原因，設定技能[{skillName}]CD失敗");
        }

    #endregion

    #region Private Methods

        private static void RequiredCD(float cd)
        {
            Assert.IsTrue(cd > 0 , $"cd: [{cd}] < 0 is invalid.");
        }

    #endregion
    }
}