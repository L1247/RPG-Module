#region

using System;
using RPGCore.Modifiers;
using Sirenix.OdinInspector;

#endregion

namespace Sample.Scripts.DataStructure
{
    [Serializable]
    public class ModifierData
    {
    #region Public Variables

        public float value;

        [LabelText("加成類型：")]
        [LabelWidth(60)]
        public ModifierType modifierType = ModifierType.Flat;

        public string statName;

    #endregion

    #region Constructor

        public ModifierData() { }

        public ModifierData(string statName , ModifierType modifierType , float value)
        {
            this.statName     = statName;
            this.modifierType = modifierType;
            this.value        = value;
        }

    #endregion
    }
}