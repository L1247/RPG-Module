#region

using System.Collections.Generic;
using NUnit.Framework;
using RPGCore.Modifiers;
using RPGCore.Stats;
using rStarUtility.Generic.TestExtensions;
using rStarUtility.Generic.TestFrameWork;

#endregion

[TestFixture]
public class StatTests : DIUnitTestFixture
{
#region Protected Variables

    protected readonly string modifierId = "modifierId";
    protected readonly string statName   = "StatName";

#endregion

#region Test Methods

    [Test(Description = "建立Stat")]
    public void _01_1_CreateStat_Default()
    {
        var stat = Stat.Create(statName , 10.5f);
        stat.Name.ShouldBe(statName);
        stat.BaseValue.ShouldBe(10.5f);
        stat.ModifierCount.ShouldBe(0);
        stat.Value.ShouldBe(10.5f);
    }

    [Test(Description = "使用Modifiers資料，建立Stat")]
    public void _01_2_CreateStat_With_Modifiers()
    {
        var modifierCreateDatas = new List<Modifier>();
        modifierCreateDatas.Add(Modifier.CreateWithoutTag(ModifierType.Flat ,       3.3f));
        modifierCreateDatas.Add(Modifier.CreateWithoutTag(ModifierType.PercentAdd , 10f));
        var stat = Stat.CreateWithModifiers(statName , 5.1f , modifierCreateDatas);
        stat.Name.ShouldBe(statName);
        stat.BaseValue.ShouldBe(5.1f);
        stat.ModifierCount.ShouldBe(2);
        stat.Value.ShouldBe(9.24f);
    }

    [Test(Description = "使用Stat，建立Stat")]
    public void _01_3_CreateStat_With_Stat()
    {
        var modifiers = new List<Modifier>();
        modifiers.Add(Modifier.CreateWithoutTag(ModifierType.Flat ,       3.3f));
        modifiers.Add(Modifier.CreateWithoutTag(ModifierType.PercentAdd , 10f));
        var otherStat = Stat.CreateWithModifiers(statName , 5.1f , modifiers);
        var stat      = Stat.CreateWithStat(otherStat);
        stat.Name.ShouldBe(statName);
        stat.ModifierCount.ShouldBe(2);
        stat.BaseValue.ShouldBe(5.1f);
        stat.Value.ShouldBe(9.24f);
    }

    [Test(Description = "套用Modifier")]
    [TestCase(                  10.7f , 5.6f , -2.5f , 30f , -10.8f ,
                       16.44f , Description = "加總正值")]
    [TestCase(                  10.9f , -1.2f , -2.5f , 5f , -10.8f ,
                       6.78f ,  Description = "加總負值")]
    public void _02_1_ApplyModifier(
            float baseValue , float mod1Value , float mod2Value , float mod3Value , float modValue4 , float expectedValue)
    {
        var stat = Stat.Create(statName , baseValue);

        stat.ApplyModifier("mod1" , ModifierType.Flat ,       mod1Value);
        stat.ApplyModifier("mod2" , ModifierType.Flat ,       mod2Value);
        stat.ApplyModifier("mod3" , ModifierType.PercentAdd , mod3Value);
        stat.ApplyModifier("mod4" , ModifierType.PercentAdd , modValue4);

        stat.Value.ShouldBe(expectedValue);
    }

    [Test(Description = "移除Modifier")]
    public void _03_RemoveModifier()
    {
        var stat = Stat.Create(statName , 10.7f);
        stat.ApplyModifier("ItemA" , ModifierType.Flat , 5.6f);
        stat.ApplyModifier("ItemB" , ModifierType.Flat , 30f);
        stat.ApplyModifier("ItemC" , ModifierType.Flat , -10.8f);

        stat.RemoveModifier("ItemA");

        stat.ModifierCount.ShouldBe(2);
        stat.Value.ShouldBe(29.9f);
    }

    [Test(Description = "移除所有Modifier")]
    public void _04_RemoveAllModifier()
    {
        var stat = Stat.Create(statName , 10.7f);
        stat.ApplyModifier("ItemA" , ModifierType.Flat , 5.6f);
        stat.ApplyModifier("ItemB" , ModifierType.Flat , 30f);
        stat.ApplyModifier("ItemC" , ModifierType.Flat , -10.8f);

        stat.RemoveAllModifier();

        stat.ModifierCount.ShouldBe(0);
        stat.Value.ShouldBe(10.7f);
    }

#endregion
}