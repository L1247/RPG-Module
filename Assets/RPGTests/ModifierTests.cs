#region

using System.Collections.Generic;
using NUnit.Framework;
using RPGCore.Modifiers;
using rStarUtility.Generic.TestExtensions;

#endregion

public class ModifierTests
{
#region Private Variables

    private const string tag = "Tag111";

#endregion

#region Test Methods

    [Test(Description = "建立一個Modifier")]
    public void _01_Create_Modifier_Default()
    {
        var modifier = Modifier.Create(tag , ModifierType.PercentAdd , 99);
        modifier.Tag.ShouldBe(tag);
        modifier.Type.ShouldBe(ModifierType.PercentAdd);
        modifier.Value.ShouldBe(99);
    }

    [Test(Description = "建立一個Modifier")]
    public void _02_Create_Modifier_Without_Tag()
    {
        var modifier = Modifier.CreateWithoutTag(ModifierType.PercentAdd , 99);
        modifier.Tag.ShouldBe(string.Empty);
        modifier.Type.ShouldBe(ModifierType.PercentAdd);
        modifier.Value.ShouldBe(99);
    }

    [Test(Description = "新增Modifier給Modifiers")]
    public void _10_Modifiers_Add_Modifier()
    {
        var modifiers = new Modifiers();

        modifiers.AddModifier(tag , ModifierType.Flat , 100);
        modifiers.GetFlatTotalValue().ShouldBe(100);
        modifiers.GetPercentAddTotalValue().ShouldBe(0);
        modifiers.Count.ShouldBe(1);

        modifiers.AddModifier(tag , ModifierType.PercentAdd , 100);
        modifiers.GetFlatTotalValue().ShouldBe(100);
        modifiers.GetPercentAddTotalValue().ShouldBe(100);
        modifiers.Count.ShouldBe(2);

        modifiers.AddModifier(ModifierType.Flat , 32);
        modifiers.Count.ShouldBe(3);
        modifiers.GetFlatTotalValue().ShouldBe(132);
        modifiers.GetPercentAddTotalValue().ShouldBe(100);
    }

    [Test(Description = "新增Modifier給Modifiers")]
    public void _11_Modifiers_Add_Modifier_By_Modifier()
    {
        var modifiers = new Modifiers();

        modifiers.AddModifier(Modifier.Create(tag , ModifierType.Flat , 100));
        modifiers.GetFlatTotalValue().ShouldBe(100);
        modifiers.GetPercentAddTotalValue().ShouldBe(0);
        modifiers.Count.ShouldBe(1);

        modifiers.AddModifiers(new List<Modifier>() { Modifier.CreateWithoutTag(ModifierType.PercentAdd , 100) });
        modifiers.GetFlatTotalValue().ShouldBe(100);
        modifiers.GetPercentAddTotalValue().ShouldBe(100);
        modifiers.Count.ShouldBe(2);
    }

    [Test(Description = "移除Modifier")]
    public void _12_Remove_Modifier()
    {
        var modifiers = new Modifiers();
        modifiers.AddModifier(Modifier.Create(tag , ModifierType.Flat , 100));

        modifiers.RemoveModifier(tag);

        modifiers.Count.ShouldBe(0);
        modifiers.GetFlatTotalValue().ShouldBe(0);
    }

    [Test(Description = "移除All Modifier")]
    public void _13_Remove_All_Modifier()
    {
        var modifiers = new Modifiers();
        modifiers.AddModifier(Modifier.Create(tag , ModifierType.Flat , 100));

        modifiers.RemoveAllModifier();

        modifiers.Count.ShouldBe(0);
        modifiers.GetFlatTotalValue().ShouldBe(0);
    }

#endregion
}