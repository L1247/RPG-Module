#region

using rStarUtility.Generic.Infrastructure;
using rStarUtility.Util;
using rStarUtility.Util.DDD.UseCase;
using rStarUtility.Util.Extensions.CSharp;
using UnityEngine;
using Zenject;

#endregion

namespace RPGCore.Items.Main
{
    public class ItemController : Controller<Item , ItemRepository>
    {
    #region Private Variables

        [Inject]
        private Item.Factory factory;

    #endregion

    #region Public Methods

        public CqrsOutput Create(string dataId , Vector2 spawnPos)
        {
            Contract.RequireString(dataId , "dataId is invalid.");
            var item = factory.Create(Item.SpawnInfo.Create(dataId , spawnPos));
            Contract.EnsureNotNull(item);
            Contract.Ensure(item.Position == spawnPos , "Position not same.");
            Contract.Ensure(item.DataId == dataId ,     "DataId not same.");
            return CqrsOutput.Create().SetId(item.Id);
        }

        public CqrsOutput PickUp(string id)
        {
            RequireId(id);
            if (Contains(id).IsFalse()) return CqrsOutput.Create().Fail().SetMessage($"找不到Id[{id}]");
            var item = GetEntity(id);
            item.PickUp();
            return CqrsOutput.Create().SetId(id);
        }

    #endregion
    }
}