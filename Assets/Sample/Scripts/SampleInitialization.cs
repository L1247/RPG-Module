#region

using Zenject;

#endregion

namespace Sample.Scripts
{
    public class SampleInitialization : IInitializable
    {
    #region Private Variables

        [Inject]
        private PlayerCharacter.Factory factory;

    #endregion

    #region Public Methods

        public void Initialize()
        {
            var playerCharacter = factory.Create();
            playerCharacter.LogStatValue();
        }

    #endregion
    }
}