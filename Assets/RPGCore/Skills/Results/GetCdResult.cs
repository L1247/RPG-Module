#region

using rStarUtility.Util.DDD.UseCase;

#endregion

namespace RPGCore.Skills.Results
{
    public class GetCdResult : CqrsOutput<GetCdResult>
    {
    #region Public Variables

        public float baseCd;

        public float cd;

    #endregion
    }
}