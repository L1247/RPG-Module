#region

using System;
using Sirenix.OdinInspector;
using UnityEngine;

#endregion

namespace Sample.Scripts.DataStructure
{
    [Serializable]
    public class StatData
    {
    #region Public Variables

        [Min(0)]
        [HorizontalGroup("StatData")]
        [LabelWidth(35)]
        [LabelText("數值：")]
        public float amount;

        [HorizontalGroup("StatData" , Width = .5f)]
        [PropertyOrder(-10)]
        [ValueDropdown("@ValueList.GetStatNames" , DropdownTitle = "數值列表" , ExpandAllMenuItems = true , IsUniqueList = true ,
                       DropdownHeight = 300)]
        [LabelWidth(40)]
        [ValidateInput("@string.IsNullOrEmpty(this.name) == false" , "屬性為空")]
        [LabelText("屬性：")]
        public string name;

    #endregion

    #region Constructor

        public StatData(string name , float amount)
        {
            this.amount = amount;
            this.name   = name;
        }

        public StatData() { }

    #endregion
    }
}