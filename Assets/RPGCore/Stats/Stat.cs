#region

using System.Collections.Generic;
using RPGCore.Modifiers;
using RPGCore.Stats.Events;
using rStarUtility.Util.Helper;
using Zenject;

#endregion

namespace RPGCore.Stats
{
    /// <summary>
    ///     計算值
    /// </summary>
    public class Stat
    {
    #region Public Variables

        public float BaseValue { get; }

        /// <summary>
        ///     計算值
        /// </summary>
        public float Value { get; private set; }

        public int ModifierCount => modifiers.Count;

        public string Name { get; }

    #endregion

    #region Private Variables

        [Inject]
        private List<StatUpdateObserver> statUpdateObservers = new List<StatUpdateObserver>();

        [Inject]
        private List<StatCreateObserver> statCreateObservers = new List<StatCreateObserver>();

        private readonly Modifiers.Modifiers modifiers = new Modifiers.Modifiers();

    #endregion

    #region Constructor

        private Stat(string name , float baseValue , List<Modifier> modifiers)
        {
            Name      = name;
            BaseValue = baseValue;
            modifiers.ForEach(data => ApplyModifier(data.Tag , data.Type , data.Value));
            if (ModifierCount == 0) Value = baseValue;
        }

    #endregion

    #region Public Methods

        public void ApplyModifier(string modifierTag , ModifierType modifierType , float modifierValue)
        {
            modifiers.AddModifier(modifierTag , modifierType , modifierValue);
            Value = CalculateValue();
        }

        public static Stat Create(string name , float baseValue)
        {
            return CreateWithModifiers(name , baseValue , new List<Modifier>());
        }

        public static Stat CreateWithModifiers(string statName , float baseValue , List<Modifier> modifiers)
        {
            return new Stat(statName , baseValue , modifiers);
        }

        public static Stat CreateWithStat(Stat otherStat)
        {
            return CreateWithModifiers(otherStat.Name , otherStat.BaseValue , otherStat.modifiers.ModifierList);
        }

        public void RemoveAllModifier()
        {
            modifiers.RemoveAllModifier();
            Value = CalculateValue();
        }

        public void RemoveModifier(string modifierTag)
        {
            modifiers.RemoveModifier(modifierTag);
            Value = CalculateValue();
        }

    #endregion

    #region Private Methods

        private float CalculateValue()
        {
            if (ModifierCount == 0) return BaseValue;
            var flatTotalValue = modifiers.GetFlatTotalValue();
            var percentValue   = modifiers.GetPercentAddTotalValue();
            percentValue /= 100f;
            // PercentAdd加總進位後
            // percentValue = GetRoundValue(percentValue);
            var calculatedValue = (BaseValue + flatTotalValue) * (1 + percentValue);
            // Debug.Log($"{flatTotalValue} , {percentValue} , {calculatedValue}");
            // 最終值低於零時_等於零
            // var roundFinalValue = GetRoundValue(calculatedFinalValue);
            return calculatedValue;
            // var valueChanged = lastFinalValue != Value;
            // if (valueChanged) statUpdateObservers.ForEach(_ => _.OnStatValueUpdated(Name , Value));
        }

        private float GetRoundValue(float percentValue)
        {
            return Math.GetRoundValue(percentValue , 2);
        }

    #endregion
    }
}