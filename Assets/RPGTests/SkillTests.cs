#region

using NUnit.Framework;
using RPGCore.Skills;
using rStarUtility.Generic.TestExtensions;
using rStarUtility.Generic.TestFrameWork;

#endregion

public class SkillTests : TestFixture_DI_Log
{
#region Protected Variables

    protected static string skillId => "Skill001";

#endregion

#region Test Methods

    [Test(Description = "建立技能")]
    [TestCase(1 ,  1 , Description = "正常案例，CD大於0")]
    [TestCase(-1 , 0 , Description = "容許案例，CD小於0")]
    public void _01_Create_Skill(float cd , float expectedCd)
    {
        var skill = Create_Skill(1);
        skill.Name.ShouldBe(skillId);
        skill.Id.ShouldNotBeNull();
        skill.Id.ShouldBe(skillId);
        skill.Cd.ShouldBe(1);
        skill.RemainingCd.ShouldBe(1);
        skill.ElapsedTime.ShouldBe(0);
    }

    [Test(Description = "倒數Skill時間")]
    public void _02_Tick_Skill_Time()
    {
        var skill = Create_Skill(1);
        skill.TickTime(0.5f);
        skill.RemainingCd.ShouldBe(0.5f);
        skill.ElapsedTime.ShouldBe(0.5f);

        skill.TickTime(0.5f);
        // cause skill reset.
        skill.RemainingCd.ShouldBe(1);
        skill.ElapsedTime.ShouldBe(0);
    }

    [Test(Description = "重置剩餘CD")]
    public void _03_Reset_Skill_Remaining_CD()
    {
        var skill = Create_Skill(1);
        skill.TickTime(100);

        skill.ResetRemainingCD();

        skill.Cd.ShouldBe(1);
        skill.RemainingCd.ShouldBe(1);
        skill.ElapsedTime.ShouldBe(0);
    }

    [Test(Description = "設定CD，如果剩餘CD超過CD，則重置剩餘CD")]
    [TestCase(0.5f , 0.5f , Description = "剩餘CD小於等於CD，不重置剩餘CD")]
    [TestCase(0.6f , 0.5f , Description = "剩餘CD大於CD，重置剩餘CD")]
    public void _04_Set_CD(float remainingCd , float expected_RemainingCd)
    {
        var skill = Create_Skill_With_RemainingCD(1 , remainingCd);

        skill.SetCD(0.5f);

        skill.BaseCD.ShouldBe(1);
        skill.Cd.ShouldBe(0.5f);
        skill.RemainingCd.ShouldBe(expected_RemainingCd , "RemainingCd not equal.");
    }

#endregion

#region Protected Methods

    protected Skill Create_Skill(float cd , bool noCd = false)
    {
        var spawnData = new Skill.SpawnData(skillId , cd , noCd);
        Container.Bind<Skill>().AsSingle().WithArguments(spawnData);
        return Resolve<Skill>();
    }

#endregion

#region Private Methods

    private Skill Create_Skill_With_RemainingCD(float cd , float remainingCd)
    {
        Bind_Instance(remainingCd , "RemainingCd");
        var skill = Create_Skill(cd);
        return skill;
    }

#endregion
}