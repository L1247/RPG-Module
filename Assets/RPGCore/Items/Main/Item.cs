#region

using System;
using System.Collections.Generic;
using RPGCore.Items.Events;
using rStarUtility.Generic.Infrastructure;
using rStarUtility.Util.Helper;
using UnityEngine;
using Zenject;

#endregion

namespace RPGCore.Items.Main
{
    public class Item : IPoolable<Item.SpawnInfo , IMemoryPool> , IDisposable , IEntity<string>
    {
    #region Public Variables

        public bool Disposed { get; private set; }

        [Inject(Optional = true , Id = "Item.DataId")]
        public string DataId { get; private set; }

        [Inject(Optional = true , Id = "Item.Id")]
        public string Id { get; private set; } = "Default";

        public Vector2 Position { get; private set; }

    #endregion

    #region Private Variables

        [Inject(Optional = true)]
        private IMemoryPool pool;

        [Inject]
        private List<SpawnObserver> spawnObservers = new List<SpawnObserver>();

        [Inject]
        private List<DeSpawnObserver> deSpawnObservers = new List<DeSpawnObserver>();

        [Inject]
        private List<PickUpObserver> pickUpObservers = new List<PickUpObserver>();

    #endregion

    #region Public Methods

        public void Dispose()
        {
            if (Disposed) return;
            pool?.Despawn(this);
            Disposed = true;
        }

        public void OnDespawned()
        {
            deSpawnObservers.ForEach(_ => _.OnDeSpawn(Id));
        }

        public void OnSpawned(SpawnInfo spawnInfo , IMemoryPool pool = null)
        {
            this.pool = pool;
            Disposed  = false;
            Position  = spawnInfo.SpawnPos;
            DataId    = spawnInfo.DataId;
            Id        = GUIDHelper.GetRandomGuid();
            spawnObservers.ForEach(_ => _.OnSpawn(Id , DataId , Position));
        }

        public void PickUp()
        {
            if (Disposed) return;
            Dispose();
            pickUpObservers.ForEach(_ => _.OnItemPickUp(Id , DataId));
        }

    #endregion

    #region Nested Types

        public class Factory : PlaceholderFactory<SpawnInfo , Item> { }

        public class Pool : PoolableMemoryPool<SpawnInfo , IMemoryPool , Item> { }

        public class SpawnInfo
        {
        #region Public Variables

            public string DataId { get; }

            public Vector2 SpawnPos { get; }

        #endregion

        #region Constructor

            private SpawnInfo(Vector2 spawnPos , string dataId)
            {
                SpawnPos = spawnPos;
                DataId   = dataId;
            }

        #endregion

        #region Public Methods

            public static SpawnInfo Create(string dataId , Vector2 spawnPos)
            {
                return new SpawnInfo(spawnPos , dataId);
            }

            public static SpawnInfo CreateEmpty()
            {
                return new SpawnInfo(Vector2.zero , string.Empty);
            }

        #endregion
        }

    #endregion
    }
}