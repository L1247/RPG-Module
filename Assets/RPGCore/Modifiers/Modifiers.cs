#region

using System.Collections.Generic;
using System.Linq;
using rStarUtility.Util.Extensions.CSharp;

#endregion

namespace RPGCore.Modifiers
{
    public class Modifiers
    {
    #region Public Variables

        public int Count => ModifierList.Count;

        public List<Modifier> ModifierList { get; }

    #endregion

    #region Constructor

        public Modifiers()
        {
            ModifierList = new List<Modifier>();
        }

    #endregion

    #region Public Methods

        public void AddModifier(string tag , ModifierType type , float value)
        {
            var modifier = Modifier.Create(tag , type , value);
            ModifierList.Add(modifier);
        }

        public void AddModifier(ModifierType type , float value)
        {
            var modifier = Modifier.CreateWithoutTag(type , value);
            ModifierList.Add(modifier);
        }

        public void AddModifier(Modifier modifier)
        {
            ModifierList.Add(modifier);
        }

        public void AddModifiers(List<Modifier> modifiers)
        {
            modifiers.ForEach(AddModifier);
        }

        public float GetFlatTotalValue()
        {
            var totalValue    = 0f;
            var flatModifiers = ModifierList.Where(modifier => modifier.Type == ModifierType.Flat);
            flatModifiers.ForEach(modifier => totalValue += modifier.Value);
            return totalValue;
        }

        public float GetPercentAddTotalValue()
        {
            var totalValue          = 0f;
            var percentAddModifiers = ModifierList.Where(modifier => modifier.Type == ModifierType.PercentAdd);
            percentAddModifiers.ForEach(modifier => totalValue += modifier.Value);
            return totalValue;
        }

        public void RemoveAllModifier()
        {
            ModifierList.Clear();
        }

        public void RemoveModifier(string modifierTag)
        {
            ModifierList.RemoveAll(modifier => modifier.Tag == modifierTag);
        }

    #endregion
    }
}