namespace RPGCore.Items.Events
{
    public interface DeSpawnObserver
    {
    #region Public Methods

        void OnDeSpawn(string id);

    #endregion
    }
}