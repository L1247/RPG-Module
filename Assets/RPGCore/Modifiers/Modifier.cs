#region

#endregion

#region

#endregion

namespace RPGCore.Modifiers
{
    public struct Modifier
    {
    #region Public Variables

        public float        Value { get; }
        public ModifierType Type  { get; }
        public string       Tag   { get; }

    #endregion

    #region Constructor

        private Modifier(string tag , ModifierType type , float value)
        {
            Tag   = tag;
            Type  = type;
            Value = value;
        }

    #endregion

    #region Public Methods

        public static Modifier Create(string tag , ModifierType type , float value)
        {
            return new Modifier(tag , type , value);
        }

        public static Modifier CreateWithoutTag(ModifierType type , float value)
        {
            return Create(string.Empty , type , value);
        }

    #endregion
    }
}